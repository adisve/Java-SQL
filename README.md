## Connect JDBC to your localhost SQL server

A program which will connect your java application to your local SQL server, allowing you to query information.

## Folder Structure

The workspace contains two folders by default, where:

- `src`: the folder to maintain sources
- `lib`: the folder to maintain dependencies

Meanwhile, the compiled output files will be generated in the `bin` folder by default.
